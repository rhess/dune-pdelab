// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_PDELAB_TIMESTEPPINGPARAMETERINTERFACE_HH
#define DUNE_PDELAB_TIMESTEPPINGPARAMETERINTERFACE_HH

#warning This file is deprecated and will be removed after the DUNE-PDELab 2.4 release! Include the header dune/pdelab/instationary/onestepparameter.hh instead!

#include <dune/pdelab/instationary/onestepparameter.hh>

#endif
