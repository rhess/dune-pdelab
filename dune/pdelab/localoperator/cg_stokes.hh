// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_PDELAB_CGSTOKES_HH
#define DUNE_PDELAB_CGSTOKES_HH

#warning This file is deprecated and will be removed after the DUNE-PDELab 2.4 release! Include the header dune/pdelab/localoperator/taylorhoodnavierstokes.hh instead!

#include <dune/pdelab/localoperator/taylorhoodnavierstokes.hh>

#endif
