install(FILES callswitch.hh
              cg_stokes.hh
              convectiondiffusion.hh
              convectiondiffusiondg.hh
              convectiondiffusionfem.hh
              convectiondiffusionparameter.hh
              defaultimp.hh
              dgnavierstokes.hh
              dgnavierstokesparameter.hh
              dgnavierstokesvelvecfem.hh
              dgparameter.hh
              diffusion.hh
              diffusionccfv.hh
              diffusiondg.hh
              diffusionmixed.hh
              diffusionparam.hh
              electrodynamic.hh
              errorindicatordg.hh
              eval.hh
              flags.hh
              idefault.hh
              interface.hh
              l2.hh
              l2volumefunctional.hh
              laplace.hh
              laplacedirichletccfv.hh
              laplacedirichletp12d.hh
              linearelasticity.hh
              linearelasticityparameter.hh
              linearacousticsdg.hh
              linearacousticsparameter.hh
              maxwelldg.hh
              maxwellparameter.hh
              navierstokesmass.hh
              pattern.hh
              poisson.hh
              scaled.hh
              stokesdg.hh
              sum.hh
              stokesdgparameter.hh
              stokesparameter.hh
              stokesdg_vecfem.hh
              taylorhoodnavierstokes.hh
              transportccfv.hh
              twophaseccfv.hh
              weightedsum.hh
              zero.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/pdelab/localoperator)
